from abc import ABC,abstractmethod

class A(ABC):
	@abstractmethod
	def name():
		return 'A'
class Aprim(A):
	def dupa():
		pass
	def name():
		super(A,A()).name()
		return 'Aprim'
a = A()
print(A.name())
print(Aprim.name())
if issubclass(ABC,Aprim):
	print('Tak')
A.register(list)
if isinstance([],A):
	print('Yes')
