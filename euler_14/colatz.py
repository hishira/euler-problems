from operator import itemgetter

def function(number):
    i = 0
    num = number
    while number > 1:
        if number%2 == 0:
            number = number/2
            i+=1
        else:
            number = 3*number+1
            i+=1
    return {'i':i,'number':num}

lista = []
for i in range(10000,1000000):
    lista.append(function(i))

print(len(lista))
lista.sort(key=itemgetter('i'),reverse=True)
print(lista[0])