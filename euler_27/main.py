import math

def isPrime(number:int):
    if number == 0 or number == 1:
        return False
    elif number == 2:
        return True
    toHow = int(math.sqrt(number))
    for i in range(2,toHow+1):
        if number%i ==0:
            return False
    return True

def mainFunction():
    #BruteForce
    n = 0
    count = 0
    maxcount = 0    
    amax = 0
    bmax = 0
    tmpa = -1000
    tmpb = -1000
    for a in range(-999,1000):
        for b in range(-1000,1001):
            number = abs(n**2 + a*n +b)
            while isPrime(number):
                count+=1
                n+=1
                number = abs(n**2 + a*n +b)            
            if count > maxcount:
                maxcount = count
                amax = a
                bmax = b
            count = 0
            n = 0        
        n = 0
        count = 0
    return amax * bmax

if __name__ == '__main__':
    print(mainFunction())
