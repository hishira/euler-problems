import datetime
from math import sqrt
def func(a:int,b:int) ->int:
    lista = set()
    for i in range(2,a+1):
        for k in range(2,b+1):
            lista.add(i**k)
    return len(lista)

def func2(a:int,b:int) ->int:
    la = set()
    lb = set()
    count = 0
    for i in range(2,a+1):
        la.add(i)
        for k in range(2,b+1):
            if b in la and i in lb:
                continue
            count += 1
            lb.add(k)
    return count
#Time measurment

start = datetime.datetime.now()
print(func(100,100))
print(datetime.datetime.now() - start)
'''
start = datetime.datetime.now()
print(func2(100,100))
print(datetime.datetime.now() - start)
'''
