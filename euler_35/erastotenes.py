import datetime
from math import sqrt
def erastotenes_sieve(n:int) ->list:
    lista = [True for x in range(0,n+1)]
    lista[0],lista[1] = False,False
    pierwiastek = int(sqrt(n))
    for i in range(2,pierwiastek):
        if lista[i]:
            w = i*i
            while w < n+1:
                lista[w] = False
                w+=i
    return [i for i,x in enumerate(lista) if x is not False ]

def erastotenes_sieve_worst(n:int) ->list:
    lista = [True for x in range(0,n+1)]
    lista[0],lista[1] = False,False
    pierwiastek = int(sqrt(n))
    for i in range(2,pierwiastek):
        w = i*i
        while w < n+1:
            lista[w] = False
            w+=i

    return len([i for i,x in enumerate(lista) if x is not False ])

if __name__ == '__main__':
    print('Normalny:')
    start = datetime.datetime.now()
    print(erastotenes_sieve(1000000))
    print(datetime.datetime.now() - start)
    print('Wiecej obliczen:')
    start = datetime.datetime.now()
    print(erastotenes_sieve_worst(1000000))
    print(datetime.datetime.now() - start)
