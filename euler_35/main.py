from erastotenes import erastotenes_sieve
import datetime
def problem(n):
    lista = erastotenes_sieve(n)
    parzyste = ['0','2','4','6','8','5']
    counter = 0
    for i in lista:
        dupa = False
        num_str = str(i) #String w postaci liczby
        
        if len(num_str) == 1:
            counter+=1
            continue
        for q in parzyste:
            if q in num_str[1:]:
                dupa = True
                break
        if dupa:
            continue
        leng = len(num_str)
        k = 0
        arr = []
        while k < leng - 1:
            char = num_str[0]
            num_str = num_str[1:]
            num_str+=char
            arr.append(int(num_str))
            k+=1
        if all(elem in lista for elem in arr):
            counter+=1
    return counter

start = datetime.datetime.now()
print(problem(1000000))
print('Czas:',datetime.datetime.now() - start)
#print(len([2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97]))
'''
num_str = '456765'
lista = erastotenes_sieve(100)
dupa = []
for i in lista:
    k = 0
    num_str = str(i)
    leng = len(num_str)
    arr = []
    print(i, end=' ')
    while k < leng -1:
        char = num_str[0]
        num_str= num_str[1:]
        num_str+=char
        arr.append(int(num_str))
        print(arr[-1],end=' ')
        k+=1
    if all(elem in lista for elem in arr):
        dupa.append(i)
    print()

print(dupa)
'''
