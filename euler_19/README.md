# Factorial digit sum
## Problem 20

n! means n × (n − 1) × ... × 3 × 2 × 1<br>

For example, 10! = 10 × 9 × ... × 3 × 2 × 1 = 3628800,<br>
and the sum of the digits in the number 10! is 3 + 6 + 2 + 8 + 8 + 0 + 0 = 27.
<br>
Find the sum of the digits in the number 100!
