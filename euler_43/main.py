def function():
    divideby2 = []
    divideby3 = []
    divideby5 = []
    divideby7 = []
    divideby11 = []
    divideby13 = []
    divideby17 = []
    for i in range(1,1000):
        if i%2 == 0:
            divideby2.append(str(i).zfill(3))
        if i%3 == 0:
            divideby3.append(str(i).zfill(3))
        if i%5 == 0:
            divideby5.append(str(i).zfill(3))
        if i%7 == 0:
            divideby7.append(str(i).zfill(3))
        if i%11 == 0:
            divideby11.append(str(i).zfill(3))
        if i%13 == 0:
            divideby13.append(str(i).zfill(3))
        if i%17 == 0:
            divideby17.append(str(i).zfill(3))

function()
print('Done')
