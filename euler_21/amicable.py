import time
def d(number):
    return sum(list(i for i in range(1,int(number/2+1)) if number%i==0))

start = time.time()
print(sum(list(x for x in range(1,10000) if (x==d(d(x)) and x!=d(x)))))
print(time.time()-start)