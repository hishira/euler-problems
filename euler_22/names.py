filename = 'names.txt'

def func(name):
    suma = 0
    for char in name:
        suma+=ord(char) - 64
    return suma

with open(filename) as file_obj:
    reader = file_obj.read()
reader =  reader.replace('"','').split(',')
suma = 0 
i = 1
reader.sort()
for name in reader:
    suma += func(name) * i
    i+=1
print(suma)
